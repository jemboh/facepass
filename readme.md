# FacePass

## The Brief

Implement a single page application (nodejs in the backend and angular in the front-end) that presents a form to the user to fill out. The form should ask for basic profile information and allow the user to upload a photo (optional)

The backend should then validate (using an API such as http://apicloud.me/apis/facerect/demo/) that the photo contains a face, and that the user's IP is in the UK (using an API such as https://freegeoip.net/?q=104.59.124.28). If it is, send an email to the user confirming their account is created, otherwise display an error message

## Getting Started

The app is split in to two folders, `api` is the Node server and `front-end` is all Angular.

You will need to create a new environment variable for the mashape API key to use the face recognition API. This is named `MASHAPE_API_KEY` (app.js:37).

I've created a throwaway email address for nodemailer to use, hence the use of username and password in the code!

### Run the tests!
You'll need to open a few tabs in the terminal for this:

- `cd api` then `npm install` then `npm start`
- In a new tab, `cd front-end/app` start the Angular app with `python -m SimpleHTTPServer 9000`
- In a new tab, from any directory `webdriver-manager update` then `webdriver-manager start`
- In a new tab, `cd front-end/test` then `protractor protractor.conf.js`

## Challenges

My biggest challenge was time, or lack of. **It's not an excuse** but I feel I should re-iterate that here. There are things I've done here that I probably wouldn't do in a production environment and that's purely so I could submit this in a reasonable time.

Getting setup with Protractor took me a while but once it was up and running it was ok.

I really struggled with testing the external API calls for facial recognition and geoip. I think I know _what_ to do but just struggling with the implentation. In the end I gave up and just wrote them out and they seem to be working ok. I think my e2e tests cover the use cases pretty well. 

The lack of back-end tests is poor. See above.

However, this was a fun and challenging test. It was nice to use Protractor as I haven't before. I would have liked to have worked out how to test promises correctly but alas, not enough time. 


## To-do

- Clear out the image preview after submission
- Consistent size of preview image
- Re-write API calls with tests
- Refactor node `app.js` file as it's pretty cluttered
- much much more...
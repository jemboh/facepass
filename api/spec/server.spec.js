var request = require('request')

describe('The Server', function() {
  describe('GET /', function() {
    it('returns a 200 status code', function(done) {
      request.get('http://localhost:9000', function(error, response, body) {
        expect(response.statusCode).toBe(200)
        done()
      })
    })
  })
})

var express = require('express');
var app = express();
var port = process.env.PORT || 3333;
var multer  = require('multer');
var cors = require('cors');
var fs = require('fs');
var unirest = require('unirest');
var Q = require('q');
var nodemailer = require('nodemailer');

var transporter = nodemailer.createTransport('smtps://facepasstest%40gmail.com:oldstreetroundabout@smtp.gmail.com');

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, __dirname + '/uploads');
  },
  filename: function (req, file, cb) {
    cb(null, file.fieldname + '-' + Date.now() + '.' + file.mimetype.split('/')[1]);
  }
});

var upload = multer({ storage: storage });

app.use(cors());
app.enable('trust proxy');

function faceRecognition(req) {
  var deferred = Q.defer();

  unirest
    .post('https://apicloud-facerect.p.mashape.com/process-file.json')
    .header('X-Mashape-Key', process.env.MASHAPE_API_KEY)
    .attach('image', fs.createReadStream(req.file.path))
    .end(function(response) {
      if(response.status !== 200) {
        deferred.reject(response.status);
      } else if(response.status === 200 && response.body.faces.length === 0) {
        deferred.reject();
      } else {
        deferred.resolve(true);
      }
    })

  return deferred.promise;
}

function geoIPCheck() {
  var deferred = Q.defer();

  unirest
    .get('http://freegeoip.net/json/')
    .end(function(response) {
      if(response.error) {
        deferred.reject();
      } else if(response.body.country_code !== 'GB') {
        deferred.reject();
      } else {
        deferred.resolve(true);
      }
    })

  return deferred.promise;
}

function sendEmail(body) {
  var mailOptions = {
    from: '"facepass" <facepasstest@gmail.com>',
    to: body.email,
    subject: 'Welcome to facepass!',
    text: body.firstName + ', welcome to facepass!',
    html: body.firstName + ', welcome to facepass!'
  };

  transporter.sendMail(mailOptions, function(error, info){
    if(error) return console.log(error);
    console.log('Message sent: ' + info.response);
  });
}

app.get('/', function(req, res) {
  res.status(200);
})


app.post('/photo_upload', upload.single('file'), function(req, res) {
  var validProfilePhoto = faceRecognition(req);
  var validLocation = geoIPCheck();

  Q.allSettled([validProfilePhoto, validLocation]).then(function(response) {
    // NOTE: The geoip API can be a bit flakey.
    // If you want to force the response, just change 'response[1].value' to be true/false
    // depending on if you want to be in the UK or not.
    if(response[0].value && response[1].value) {
      res.send('Success! Valid photo and valid location. Email sent.')
      sendEmail(req.body)
    } else {
      res.status(400).send('Error! It seems something went wrong. '
             + 'Please upload a valid profile photo and ensure you are in the UK. '
             + 'If you meet the criteria, we may be having some problems so try again soon!')
    }
  })
})

app.listen(port, function() {
  console.log('Server started on http://localhost:%s', port)
})

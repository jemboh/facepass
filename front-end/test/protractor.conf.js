exports.config = {
  seleniumAddress: 'http://localhost:4444/wd/hub',
  framework: 'jasmine',
  specs: ['e2e/**/*.spec.js'],
  baseUrl: 'http://localhost:9000',
  capabilities: {
    'browserName': 'chrome'
  }, 
  jasmineNodeOpts: {
    showColors: true
  },
  allScriptsTimeout: 30000
}
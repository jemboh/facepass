var IndexPage = require('./index.po.js')
var indexPage;

describe('Facepass Index Page', function() {

  beforeEach(function() {
    indexPage = new IndexPage()
    browser.get('/')
  })

  it('should have a title', function() {
    expect(browser.getTitle()).toEqual('facepass')
  })

  it('should have a heading', function() {
    expect(indexPage.getHeading()).toBe('facepass')
  })

  describe('Registration Form', function() {
    it('should be able to enter first name', function() {
      indexPage.setFirstName('Jeremy')

      expect(indexPage.getFirstName()).toBe('Jeremy')
    })

    it('should be able to enter last name', function() {
      indexPage.setLastName('Marer')

      expect(indexPage.getLastName()).toBe('Marer')
    })

    it('should be able to enter their email', function() {
      indexPage.setEmailAddress('jeremy@mailinator.com')

      expect(indexPage.getEmailAddress()).toBe('jeremy@mailinator.com')
    })

    it('should be able to select a profile photo and display a preview of it', function() {
      indexPage.selectValidProfilePhoto()

      expect($('#file-upload-preview').isDisplayed()).toBe(true)
    })

    it('submit button should be disabled', function() {
      expect(indexPage.submitButton.getAttribute('disabled')).toBeTruthy()
    })

    it('submitting a valid photo and located in the UK returns a success message', function() {
      indexPage.setFirstName('Jeremy')
      indexPage.setLastName('Marer')
      indexPage.setEmailAddress('jeremy@mailinator.com')
      indexPage.selectValidProfilePhoto()
      indexPage.submitRegistrationForm()

      expect(indexPage.getFormMessage()).toContain('Success!')
    })

    it('submitting an invalid photo but in the UK should return an error message', function() {
      indexPage.setFirstName('Jeremy')
      indexPage.setLastName('Marer')
      indexPage.setEmailAddress('jeremy@mailinator.com')    
      indexPage.selectInvalidProfilePhoto()
      indexPage.submitRegistrationForm()

      expect(indexPage.getFormMessage()).toContain('Error!')
    });

    it('should display error message on first name if under three characters', function() {
      indexPage.setFirstName('Je')

      expect($('#firstNameError').getText()).toBe('Your first name must have a minimum of three characters')
    });

    it('should display error message on last name if under two characters', function() {
      indexPage.setLastName('M')

      expect($('#lastNameError').getText()).toBe('Your last name must have a minimum of two characters')
    });

    it('should display error message on email address if not a "valid" email', function() {
      indexPage.setEmailAddress('jeremy')

      expect($('#emailAddressError').getText()).toBe('Please enter a valid email address')
    });
  })
})

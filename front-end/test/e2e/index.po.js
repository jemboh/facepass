var path = require('path')

var IndexPage = function() {
  var validProfilePhotoLocation = path.join(__dirname, '/data/batman_robin.jpg')
  var invalidProfilePhotoLocation = path.join(__dirname, '/data/fruit.jpg')

  var heading = element(by.tagName('h1'))
  var firstName = element(by.model('main.user.firstName'))
  var lastName = element(by.model('main.user.lastName'))
  var emailAddress = element(by.model('main.user.emailAddress'))
  var fileUploadField = element(by.id('file-upload'))
  var fileUploadPreview = element(by.id('file-upload-preview'))
  var formMessage = element(by.id('formMessage'))
  this.submitButton = element(by.id('registration-submit'))
  this.profilePhotoFileName = 'batman_robin.jpg'


  this.getHeading = function() {
    return heading.getText()
  }

  this.getFirstName = function() {
    return firstName.getAttribute('value')
  }

  this.setFirstName = function(name) {
    firstName.sendKeys(name)
  }

  this.getLastName = function() {
    return lastName.getAttribute('value')
  }

  this.setLastName = function(name) {
    lastName.sendKeys(name)
  }

  this.getEmailAddress = function() {
    return emailAddress.getAttribute('value') 
  }

  this.setEmailAddress = function(email) {
    emailAddress.sendKeys(email)
  }

  this.selectValidProfilePhoto = function(filename) {
    fileUploadField.sendKeys(validProfilePhotoLocation)
  }

  this.selectInvalidProfilePhoto = function(filename) {
    fileUploadField.sendKeys(invalidProfilePhotoLocation)
  }

  this.submitRegistrationForm = function() {
    this.submitButton.click()
  }

  this.getFormMessage = function() {
    return formMessage.getText()
  }
}

module.exports = IndexPage;

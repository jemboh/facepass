angular
  .module('facepassApp')
  .controller('MainController', MainController)

MainController.$inject = ['$scope', 'Upload']

function MainController($scope, Upload) {
  var self = this;

  this.heading = 'facepass';
  this.user = {};
  this.file;
  this.response;

  this.submit = function() {
    self.upload(self.file);
  }

  this.upload = function() {
    Upload.upload({
      url: 'http://localhost:3333/photo_upload',
      data: { 
        file: self.file,
        firstName: self.user.firstName,
        lastName: self.user.lastName,
        email: self.user.emailAddress
      }
    }).then(function (response) {
      self.response = {
        status: 'success',
        message: response.data
      }
      // reset form
      self.registerForm.$setPristine();
      self.registerForm.$setUntouched();
      self.user = {};
      self.file;
    }, function (response) {
      self.response = {
        status: 'error',
        message: response.data
      }
    })
  }
}
